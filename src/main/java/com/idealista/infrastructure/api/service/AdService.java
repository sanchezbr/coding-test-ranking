package com.idealista.infrastructure.api.service;

import com.idealista.infrastructure.api.entity.PublicAd;
import com.idealista.infrastructure.api.entity.QualityAd;

import java.util.List;

public interface AdService {

    public void calculateAdsScore();

    public List<QualityAd> getAdsByQuality();

    public List<PublicAd> getPublicAds();
}
