package com.idealista.infrastructure.api.service;


import com.idealista.infrastructure.api.entity.PublicAd;
import com.idealista.infrastructure.api.entity.QualityAd;
import com.idealista.infrastructure.persistence.entity.AdVO;
import com.idealista.infrastructure.persistence.repository.InMemoryPersistence;
import groovy.util.logging.Slf4j;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
@AllArgsConstructor
public class AdServiceImplementation implements AdService {

    private InMemoryPersistence inMemoryPersistence;

    private final List<String> keyWords = Arrays.asList("Luminoso", "Nuevo", "Céntrico", "Reformado", "Ático");

    @Override
    public void calculateAdsScore(){
        List<AdVO> adVOList = inMemoryPersistence.findAllAds();

        adVOList.stream().forEach((ad)-> {
            int score = calculateScore(ad);

            if (score < 40){
                ad.setIrrelevantSince(new Date());
            }
            score = adjustScore(score);
            ad.setScore(score);
        });
    }

    @Override
    public List<QualityAd> getAdsByQuality(){
        return inMemoryPersistence.findAllIrrelevantAds().stream().map(ad -> QualityAd.fromVO(ad)).collect(Collectors.toList());
    }

    @Override
    public List<PublicAd> getPublicAds(){
        return inMemoryPersistence.findAllAdsByScore().stream().map(ad -> PublicAd.fromVO(ad)).collect(Collectors.toList());
    }

    private int calculateScore(AdVO adVO){
        int finalScore = adHasPhoto(adVO) + adHasDescription(adVO) + adIsComplete(adVO);

        return finalScore;

    }

    private int adjustScore(int score){
        if (score < 0){
            score = 0;
        }
        else if (score > 100){
            score = 100;
        }

        return score;

    }

    private int adHasPhoto(AdVO adVO){
        int score = 0;
        if(adVO.getPictures().isEmpty()){
            score -= 10;
        }
        else{
            for(int pictureId: adVO.getPictures()){
                if(inMemoryPersistence.getPictureById(pictureId).getQuality().equals("HD")){
                    score += 20;
                }
                else{
                    score += 10;
                }
            }
        }
        return score;

    }

    private int adHasDescription(AdVO adVO){
        int score = 0;

        if(!adVO.getDescription().isEmpty()){
            score += 5 + adDescriptionSize(adVO.getTypology(), adVO.getDescription() + wordsAppearInDescription(adVO.getDescription()));
        }
        return score;
    }

    private int adDescriptionSize(String typology, String description){
        int score = 0;
        int descriptionSize = description.length();

        if(typology.equals("FLAT")){
            if (descriptionSize >= 20 && descriptionSize <= 49){
                score += 10;
            }
            else if (descriptionSize >= 50){
                score += 30;
            }
        }
        else if(typology.equals("CHALET")){
            if (descriptionSize >= 50){
                score += 20;
            }
        }
        return score;
    }

    private int wordsAppearInDescription(String description){
        int score = 0;

        for(String keyWord: keyWords){
            if (description.contains(keyWord)){
                score += 5;
            }
        }
        return score;
    }

    private int adIsComplete(AdVO adVO){
        int score = 0;

        boolean isComplete = adVO.getPictures().size() >= 1 &&
                (isFlatAndHasHouseSize(adVO) || isChaletAndHasHouseAndGardenSize(adVO) || isGarageAndHasHouseSize(adVO));

        if (isComplete){
            score += 40;
        }

        return score;
    }

    private boolean isFlatAndHasHouseSize(AdVO adVO){
        return !adVO.getDescription().isEmpty() && adVO.getTypology().equals("FLAT")  && !Optional.ofNullable(adVO.getHouseSize()).orElse(0).equals(0);
    }

    private boolean isChaletAndHasHouseAndGardenSize(AdVO adVO){
        return !adVO.getDescription().isEmpty() && adVO.getTypology().equals("CHALET") && !Optional.ofNullable(adVO.getHouseSize()).orElse(0).equals(0) && !Optional.ofNullable(adVO.getGardenSize()).orElse(0).equals(0);
    }

    private boolean isGarageAndHasHouseSize(AdVO adVO){
        return adVO.getTypology().equals("GARAGE") && !Optional.ofNullable(adVO.getHouseSize()).orElse(0).equals(0);
    }
}
