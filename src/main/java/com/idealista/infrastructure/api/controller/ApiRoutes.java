package com.idealista.infrastructure.api.controller;

final class ApiRoutes {

    private static final String MAIN_PATH = "/ads";

    static final String GET_USER_ADS = MAIN_PATH;

    static final String GET_QUALITY_ADS = MAIN_PATH + "/quality-department";

    static final String CALCULATE_SCORE = MAIN_PATH + "/calculate-score";
}
