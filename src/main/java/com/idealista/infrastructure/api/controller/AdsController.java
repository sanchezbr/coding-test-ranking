package com.idealista.infrastructure.api.controller;

import java.util.List;

import com.idealista.infrastructure.api.entity.PublicAd;
import com.idealista.infrastructure.api.entity.QualityAd;
import com.idealista.infrastructure.api.service.AdService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class AdsController {

    private final AdService adService;

    @GetMapping(path = ApiRoutes.GET_QUALITY_ADS)
    public ResponseEntity<List<QualityAd>> qualityListing() {
        return ResponseEntity.ok(adService.getAdsByQuality());
    }

    @GetMapping(path = ApiRoutes.GET_USER_ADS)
    public ResponseEntity<List<PublicAd>> publicListing() {
        return ResponseEntity.ok(adService.getPublicAds());
    }

    @PutMapping(path = ApiRoutes.CALCULATE_SCORE)
    public ResponseEntity<Void> calculateScore() {
        adService.calculateAdsScore();

        return ResponseEntity.ok().build();
    }
}
