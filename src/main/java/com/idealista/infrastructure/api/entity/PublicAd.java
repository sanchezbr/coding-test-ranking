package com.idealista.infrastructure.api.entity;

import com.idealista.infrastructure.persistence.entity.AdVO;
import com.idealista.infrastructure.persistence.repository.InMemoryPersistence;
import lombok.AllArgsConstructor;
import lombok.Builder;

import java.util.ArrayList;
import java.util.List;

@Builder
public class PublicAd {

    private Integer id;
    private String typology;
    private String description;
    private List<String> pictureUrls;
    private Integer houseSize;
    private Integer gardenSize;

    private static InMemoryPersistence inMemoryPersistence = new InMemoryPersistence();

    public static PublicAd fromVO(AdVO adVO){

        List<String> pictureUrls = new ArrayList<>();
        for (int id : adVO.getPictures()) {
            pictureUrls.add(inMemoryPersistence.getPictureById(id).getUrl());
        }

        return PublicAd.builder()
                .id(adVO.getId())
                .typology(adVO.getTypology())
                .description(adVO.getDescription())
                .pictureUrls(pictureUrls)
                .houseSize(adVO.getHouseSize())
                .gardenSize(adVO.getGardenSize())
                .build();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTypology() {
        return typology;
    }

    public void setTypology(String typology) {
        this.typology = typology;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getPictureUrls() {
        return pictureUrls;
    }

    public void setPictureUrls(List<String> pictureUrls) {
        this.pictureUrls = pictureUrls;
    }

    public Integer getHouseSize() {
        return houseSize;
    }

    public void setHouseSize(Integer houseSize) {
        this.houseSize = houseSize;
    }

    public Integer getGardenSize() {
        return gardenSize;
    }

    public void setGardenSize(Integer gardenSize) {
        this.gardenSize = gardenSize;
    }
}
