package com.idealista.infrastructure.api.exception;

public class PictureNotFoundException extends RuntimeException {

    public PictureNotFoundException(int id) {
        super("Picture has not been found for id: " + id);
    }
}
