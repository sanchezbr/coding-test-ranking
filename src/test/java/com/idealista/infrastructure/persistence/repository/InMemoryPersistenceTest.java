package com.idealista.infrastructure.persistence.repository;

import com.idealista.AbstractIntegrationTest;
import com.idealista.infrastructure.persistence.entity.AdVO;
import com.idealista.infrastructure.persistence.entity.PictureVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class InMemoryPersistenceTest extends AbstractIntegrationTest {

    @Autowired
    InMemoryPersistence inMemoryPersistence;

    @Test
    void getAdsSortedByScoreTest(){
        //given
        //when
        final List<AdVO> adVOS = inMemoryPersistence.findAllAdsByScore();
        //then
        assertThat(adVOS).isNotEmpty();
        assertThat(adVOS.size()).isEqualTo(8);
    }

    @Test
    void getIrrelevantAdsSortedByIrrelevantDateTest(){
        //given
        //when
        final List<AdVO> adVOS = inMemoryPersistence.findAllIrrelevantAds();
        //then
        assertThat(adVOS).isEmpty();
    }

    @Test
    void getAllAdsTest(){
        //given
        //when
        final List<AdVO> adVOS = inMemoryPersistence.findAllAds();
        //then
        assertThat(adVOS).isNotEmpty();
        assertThat(adVOS.size()).isEqualTo(8);
    }

    @Test
    void getPictureByIdTest(){
        //given
        final int pictureId = 1;
        final int pictureId2 = 156;
        //when
        final PictureVO pictureVO = inMemoryPersistence.getPictureById(pictureId);
        //then
        assertThat(pictureVO).isNotNull();
        assertThrows(RuntimeException.class, () -> inMemoryPersistence.getPictureById(pictureId2));
    }

}
