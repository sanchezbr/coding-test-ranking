package com.idealista.infrastructure.api.controller;

import com.idealista.AbstractIntegrationTest;
import com.idealista.infrastructure.api.entity.PublicAd;
import com.idealista.infrastructure.api.entity.QualityAd;
import org.junit.jupiter.api.Test;
import org.springframework.boot.web.server.LocalServerPort;

import java.util.List;

import static io.restassured.RestAssured.given;

public class AdsControllerTest  extends AbstractIntegrationTest{

    @LocalServerPort
    private int port;

    @Test
    void getQualityDepartmentAdsTest(){

        final List<QualityAd> ads = given().contentType("application/json")
        .port(port)
        .when()
        .get(ApiRoutes.GET_QUALITY_ADS)
        .then()
        .statusCode(200).extract().path("QualityAds");

        //Is empty because we didn't calculate the scores. That means that there are no irrelevant ads
        assertThat(ads).isEmpty();

    }

    @Test
    void getPublicAdsTest(){

        final List<PublicAd> ads = given().contentType("application/json")
        .port(port)
        .when()
        .get(ApiRoutes.GET_USER_ADS)
        .then()
        .statusCode(200).extract().path("");

        assertThat(ads).isNotEmpty();
        assertThat(ads.size()).isEqualTo(8);
    }

    @Test
    void calculateScoreTest(){

        given().contentType("application/json")
        .port(port)
        .when()
        .put(ApiRoutes.CALCULATE_SCORE)
        .then()
        .statusCode(200);
    }

    @Test
    void getQualityDepartmentAdsIntegrationTest(){

        given().contentType("application/json")
        .port(port)
        .when()
        .put(ApiRoutes.CALCULATE_SCORE)
        .then()
        .statusCode(200);

        final List<QualityAd> ads = given().contentType("application/json")
                .port(port)
                .when()
                .get(ApiRoutes.GET_QUALITY_ADS)
                .then()
                .statusCode(200).extract().path("");

        //Is not empty because we calculated the scores. That means that there are now irrelevant ads
        assertThat(ads).isNotEmpty();
        assertThat(ads.size()).isEqualTo(3);

    }

}
