package com.idealista.infrastructure.api.service;

import com.idealista.AbstractIntegrationTest;
import com.idealista.infrastructure.api.entity.PublicAd;
import com.idealista.infrastructure.api.entity.QualityAd;
import com.idealista.infrastructure.persistence.repository.InMemoryPersistence;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class AdServiceTest extends AbstractIntegrationTest {

    @Autowired
    InMemoryPersistence inMemoryPersistence;

    @Autowired
    private AdService adService;

    @Test
    void getPublicAdsTest() {
        //given
        //when
        final List<PublicAd> publicAds = adService.getPublicAds();
        //then
        assertThat(publicAds).isNotEmpty();
        assertThat(publicAds.size()).isEqualTo(8);
    }

    @Test
    void getQualityAdsTest() {
        //given
        //when
        final List<QualityAd> qualityAds = adService.getAdsByQuality();
        //then
        //Is empty because we didn't calculate the scores. That means that there are no irrelevant ads
        assertThat(qualityAds).isEmpty();
    }

    @Test
    void calculateAdsScoreTest() {
        //given
        //when
        final List<QualityAd> qualityAdsBeforeCalculatingScore = adService.getAdsByQuality();
        adService.calculateAdsScore();
        final List<QualityAd> qualityAds = adService.getAdsByQuality();
        //then
        //Is empty because we didn't calculate the scores. That means that there are no irrelevant ads
        assertThat(qualityAdsBeforeCalculatingScore).isEmpty();
        //Is not empty because we calculated the scores. That means that there are now irrelevant ads
        assertThat(qualityAds).isNotEmpty();
        assertThat(qualityAds.size()).isEqualTo(3);

    }


}
